﻿<?php $products = require('data/products.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>productTable</title>
</head>
<body>
    <table>
        <?php foreach ($products as $product): ?>
        <tr>
            <td><?= $product->get_id() ?></td>
            <td><?= $product->get_name() ?></td>
            <td><?= $product->get_price().'€' ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>
