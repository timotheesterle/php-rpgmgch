﻿<?php $users = require('data/users.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>userTable</title>
</head>
<body>
    <table>
        <?php foreach ($users as $user): ?>
        <tr>
            <td><?= $user->get_id() ?></td>
            <td><?= $user->get_email() ?></td>
            <td><?= $user->get_createdAt()['date'] ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>
