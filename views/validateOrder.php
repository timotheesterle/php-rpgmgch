﻿<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Valider la commande</title>
</head>
<body>
<ul>
<h1>Commande pour le client n°<?= $_POST['client'] ?></h1>
<?php foreach ($users[$_POST['client']]->get_cart() as $item): ?>
    <li><?= $item->get_name().', '.$item->get_price().'€' ?></li>
<?php endforeach; ?>
<p>Prix total : <?= $users[$_POST['client']]->get_billAmount().'€' ?></p>
</ul>
</body>
</html>
