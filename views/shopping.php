﻿<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shopping</title>
</head>
<body>
<form action="validateOrder.php" method="POST">

    <p>Sélectionnez un client :</p>
    <select id="client" name="client">
        <?php foreach ($users as $user): ?>
        <option value="<?= $user->get_id() ?>"><?= 'Client '.$user->get_id() ?></option>
        <?php endforeach; ?>
    </select>

    <p>Sélectionnez les produits :</p>
    <select id="choix1" name="choix1">
        <?php foreach ($products as $product): ?>
        <option value="<?= $product->get_id() ?>"><?= $product->get_name() ?></option>
        <?php endforeach; ?>
    </select>
    <select id="choix2" name="choix2">
        <?php foreach ($products as $product): ?>
        <option value="<?= $product->get_id() ?>"><?= $product->get_name() ?></option>
        <?php endforeach; ?>
    </select>
    <select id="choix3" name="choix3">
        <?php foreach ($products as $product): ?>
        <option value="<?= $product->get_id() ?>"><?= $product->get_name() ?></option>
        <?php endforeach; ?>
    </select>
    <br>
    <br>
    <button type="submit">Valider</button>

</form>
</body>
</html>
