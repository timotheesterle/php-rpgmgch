﻿<?php

class User {
    private $_id;
    private $_email;
    private $_createdAt;

    public function __construct($_id, $_email, $_createdAt) {
        $this->_id = $_id;
        $this->_email = $_email;
        $this->_createdAt = $_createdAt;
    }

    public function get_id() {
        return $this->_id;
    }

    public function get_email() {
        return $this->_email;
    }

    public function get_createdAt() {
        return (array) $this->_createdAt;
    }
}

?>
