﻿<?php

class Vegetable extends Product {
    private $_productorName;
    private $_expiresAt;

    public function __construct($_id, $_name, $_price, $_productorName, $_expiresAt) {
        parent::__construct($_id, $_name, $_price);
        $this->_productorName = $_productorName;
        $this->_expiresAt = $_expiresAt;
    }

    public function get_productorName() {
        return $this->_productorName;
    }

    public function get_expiresAt() {
        return $this->_expiresAt;
    }

    public function isFresh() {

    }
}

?>
