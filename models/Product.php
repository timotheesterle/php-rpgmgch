﻿<?php

class Product {
    private $_id;
    private $_name;
    private $_price;

    public function __construct($_id, $_name, $_price) {
        $this->_id = $_id;
        $this->_name = $_name;
        $this->_price = $_price;
    }

    public function get_id() {
        return $this->_id;
    }

    public function get_name() {
        return $this->_name;
    }

    public function get_price() {
        return $this->_price;
    }
}

?>
