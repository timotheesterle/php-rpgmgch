﻿<?php

class Cloth extends Product {
    private $_brand;

    function __construct($_id, $_name, $_price, $_brand) {
        parent::__construct($_id, $_name, $_price);
        $this->_brand = $_brand;
    }

    public function get_brand() {
        return $this->_brand;
    }

    function try() {

    }
}

?>
