﻿<?php

class Client extends User {
    private $_billAmount = 0;
    private $_cart = [];

    public function __construct($_id, $_email, $_createdAt) {
        parent::__construct($_id, $_email, $_createdAt);
    }

    public function buy() {
        echo "Achat du client n° {$this->get_id()} validé pour $this->_billAmount €<br>";
    }

    public function addProductToCart($product) {
        $this->_cart[] = $product;
        $this->_billAmount += $product->get_price();
    }

    public function get_cart() {
        return $this->_cart;
    }

    public function get_billAmount() {
        return $this->_billAmount;
    }
}

?>
