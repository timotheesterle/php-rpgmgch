﻿<?php

require('models/Product.php');
require('models/Vegetable.php');
require('models/Cloth.php');

$vegetable1 = new Vegetable(0, 'carotte', 3, 'toto', '2019-03-10');
$vegetable2 = new Vegetable(1, 'courgette', 4, 'toto', '2019-03-11');
$cloth1 = new Cloth(2, 'robe', 20, 'D&G');

return [
    $vegetable1,
    $vegetable2,
    $cloth1
];

?>
