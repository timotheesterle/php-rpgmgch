﻿<?php

require('models/User.php');
require('models/Client.php');

$client1 = new Client(0, 'paf@lechien.fr', new DateTime);
$client2 = new Client(1, 'pif@lechat.fr', new DateTime);
return [
    $client1,
    $client2
];

?>
